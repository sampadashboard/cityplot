# /tests/controllers/routes_test.rb
require 'rails_helper'

#class RoutesTest < ActionDispatch::Assertions::RoutingAssertions
=begin class RoutesTest < ActionController::IntegrationTest
  test "route test" do
    assert_generates "/graphics", { :controller => "graphic_manager", :action => "plot" }
    assert_generates "/graphics/line", { :controller => "graphic_manager", :action => "line_chart" }
    assert_generates "/graphics/bar", { :controller => "graphic_manager", :action => "bar_chart" }
  end
end
=end
RSpec.describe "routing to graphics" do
  it "routes /graphics to graphics#plot" do
    expect(get: "/graphics").to route_to(
      controller: "graphic_manager",
      action: "plot"
    )
  end
end