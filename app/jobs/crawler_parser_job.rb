require 'elasticsearch'

class CrawlerParserJob < ApplicationJob

  include ElasticSearchClient

  attr_accessor :hostname, :port, :client

  def initialize
    setup_elastic_search_conection
  end

  def fetch_last_timestamp
      query =  {"size": 1, "sort": {"date": "desc"}}
      begin
        res = @client.search index: @index_name, body: query
        timestamp = res["hits"]["hits"].first["_source"]["date"]
      rescue NoMethodError => e
        #TODO add to log
        #p e.class, e.message
        timestamp = 30.years.ago
      rescue Faraday::ConnectionFailed => e
        #TODO add to log
        # p e.class, e.message
        timestamp = nil
      rescue Exception => e
        raise e
      end

      timestamp
  end

  def perform
    p "==============================="
    p "-> PERFORMING "
    p "==============================="

    timestamp = fetch_last_timestamp
    if timestamp
      raw_json = fetch_measurements_data(timestamp)
      if raw_json
        parsed_data = parse_measurements(raw_json)
        save_measurements_data(parsed_data)
      end
    end
  end

  #TODO check if there's already saved information about the sensor before asking to
  #the platform
  def fetch_sensor_data(uuid)
    response =  InterscitySDK::ResourceCatalogManager.fetch(uuid)
    resource_data = nil

    if (response[:status] == :ok)
      resource_data = response[:resource]
      sensor_location = {lat: resource_data[:lat], lon: resource_data[:lon]}
      resource_data.delete(:lat)
      resource_data.delete(:lon)
      resource_data[:location] = sensor_location
    end

    resource_data
  end

  def fetch_measurements_data(timestamp)
    p "%s -> Fetching data from Data Collector" % self.class.name
    measures = nil
    response = InterscitySDK::DataCollectorManager.measures(timestamp)
    if response[:status] == :ok
      measures = response[:data]
    end

    measures
  end


  def parse_measurements(json_data)
    resources_measures = {}

    raise Exception.new('Cannot parse empty json in parse_measurements') if json_data.nil? 

    json_data["resources"].each do |resource|

      uuid = resource["uuid"]
      resource_data = fetch_sensor_data(uuid)
      resources_measures[uuid] = []

      resource["capabilities"].each do |capability|
        capability_name = capability.first
        measurement = {sensor: {}, measure: {} }

        capability.second.each do |measure_entry|

          measurement[:date] = measure_entry["date"]
          measure_entry.delete("date")
          measurement[:capability] = capability_name
          measurement[:measure] = measure_entry

          measurement[:sensor] = resource_data
          resources_measures[uuid].append(measurement)
        end
      end
    end

    resources_measures
  end

  def save_measurements_data(parsed_data)
    seeder = ElasticSeed.new
    seeder.send_data(parsed_data)
  end
end
