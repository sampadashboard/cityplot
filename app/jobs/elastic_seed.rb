require 'elasticsearch'

class ElasticSeed

  include ElasticSearchClient

  attr_accessor :client, :connection

  def initialize
    @type = 'measurements'
    @connection = false

    setup_elastic_search_conection
    find_or_create_index
  end

  def find_or_create_index
    begin
      unless @client.indices.exists index: @index_name
        mapping = {
          "mappings": {
            "measurements": {
              "properties": {
                "sensor": {
                  "properties": {
                    "location": {"type": "geo_point"}
                  }
                },
                "date": {"type": "date"},
                "measure": {
                  "properties": {
                    "location": {"type": "geo_point"},
                  }
                }
              }
            }
          }
        }

        @client.indices.create index: @index_name, body: mapping
      end
      @connection = true
    rescue Exception => e
      raise e
      #TODO add to log
      #p e, e.message
    end
  end

  def send_data(data)
    return unless @connection

    begin
      data.each do |resource_entry|
        resource_entry.second.each do |entry|
          @client.index index: @index_name, type: @type, body: entry
        end
      end

      @client.indices.refresh index: @index_name
    rescue Exception => e
      #TODO add to log
    end
  end

end
