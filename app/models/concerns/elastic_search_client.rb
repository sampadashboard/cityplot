require 'elasticsearch'

module ElasticSearchClient
  def setup_elastic_search_conection
    @hostname = ENV.fetch('ELASTIC_HOST', 'localhost')
    @port = ENV.fetch('ELASTIC_PORT', '9200').to_i
    @index_name = ENV.fetch('INDEX_NAME', 'data-collector')
    @client = Elasticsearch::Client.new log: true, host: @hostname, port: @port
  end
end
