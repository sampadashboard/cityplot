FROM debian:stable
RUN apt update -qy && apt install ruby bundler libxml2 zlib1g-dev libsqlite3-dev libpq-dev postgresql postgresql-contrib -yq nodejs
RUN mkdir -p /cityplot/
ADD . /cityplot/
WORKDIR /cityplot/
RUN bundle install
EXPOSE 3000
CMD [ "bundle","exec", "rails", "s", "-p", "3000", "-b", "0.0.0.0"]
