
DEFAULT_TIMEOUT = 150
DEFAULT_TIMER = 30


job_timeout = ENV.fetch('SCHDL_TIMEOUT', DEFAULT_TIMEOUT).to_i
job_timer = ENV.fetch('SCHDL_TIMER', DEFAULT_TIMER).to_i

job_timer = DEFAULT_TIMER if job_timer.zero?
job_timeout = DEFAULT_TIMEOUT if job_timeout.zero?

def shutdown_jobs?
  ENV.fetch('SCHDL_OFF', false)
end
#
# => unless defined?(Rails::Console) || Rails.env.test? || File.split($0).last == 'rake' || shutdown_jobs?
#
#  p "#"*30
#  p "Started CrawlerParserJob"
#  p "Every %s seconds" % job_timer
#  p "Timeout: %s seconds" % job_timeout
#  p "#"*30
#
#  while true
#    sleep job_timer
#    start_time = Time.now
#
#    job = CrawlerParserJob.new
#    job.perform
#
#    p "="*30
#    p "Finished! in %s seconds" % (Time.now - start_time).round(3)
#    p "="*30
#  end
#
#end
