require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
require_relative '../lib/sdk/resource_catalog_manager'
require_relative '../lib/sdk/data_collector_manager'

module Cityplot
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1


    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    [config.eager_load_paths, config.autoload_paths].each do |path|
      path << config.root.join('lib/')
    end

    config.eager_load = true
  end
end
