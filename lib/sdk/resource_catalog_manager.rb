require 'json'
require 'rest-client'

module InterscitySDK
  class ResourceCatalogManager

    ATTRIBUTES = %w(
      id uri created_at updated_at
      lat lon status collect_interval description uuid city
      neighborhood state postal_code country
    )

    ATTRIBUTES.each do |att|
      attr_accessor att.to_sym
    end

    def initialize(json_data)
      resource = json_data["data"]

      self.id = resource["id"]
      self.uri = resource["uri"]
      self.created_at = resource["created_at"]
      self.updated_at = resource["updated_at"]
      self.lat = resource["lat"]
      self.lon = resource["lon"]
      self.status = resource["status"]
      self.collect_interval = resource["collect_interval"]
      self.description = resource["description"]
      self.uuid = resource["uuid"]
      self.city = resource["city"]
      self.neighborhood = resource["neighborhood"]
      self.state = resource["state"]
      self.postal_code = resource["postal_code"]
      self.country = resource["country"]
    end

    def to_hash
      hash = {}
      ATTRIBUTES.each do |att|
        att_sym = att.to_sym  
        hash[att_sym] = self.send(att_sym)
      end
      hash
    end

    #Fetch data from sensor with specific uuid
    def self.fetch(uuid)
      object = {}

      if (uuid)
        begin

          url = ENV.fetch('PLATFORM_URL', "http://localhost:8000")
          data  = JSON.parse(RestClient.get("#{url}/catalog/resources/#{uuid}"))
          object[:status] = :ok
          object[:resource] = InterscitySDK::ResourceCatalogManager.new(data).to_hash
        rescue Exception => err
          object[:status] = :error
          object[:message] = err.message
        end
      else
        object = {status: :error, message: "uuid required"}
      end

      object
    end
  end
end
