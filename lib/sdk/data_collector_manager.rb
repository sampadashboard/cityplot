require 'json'
require 'rest-client'

module InterscitySDK
  class DataCollectorManager

    #Fetch measures from data collector
    def self.measures(timestamp=nil)
      begin 
        url = ENV.fetch('PLATFORM_URL', "http://localhost:8000")
        if timestamp
          json = JSON.parse(RestClient.post("#{url}/collector/resources/data", {"matchers":{"date.gt":timestamp}}))
        else
          json = JSON.parse(RestClient.post("#{url}/collector/resources/data", []))
        end

        {status: :ok, data: json}
      rescue Exception => e
        {status: :error, message: "could not fetch measures from the platform"}
      end
    end
  end
end
