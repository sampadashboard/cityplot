# Cityplot Development Instructions

## Developer Roles

* *Developers* are everyone that is contributing code to Cityplot.
* *Committers* are the people with direct commit access to the Cityplot source
  code. They are responsible for reviewing contributions from other developers
  and integrating them in the Cityplot code base.


## Current Commiters

* Alyssa
* Eduardo
* Fernanda
* Fernando
* Tallys


## Dependencies

- Ruby >= 2.4.2
- Rails >= 5.1
- Chartkick >= 2.2.4


## Docker Environment

Install docker and docker-compose.

```docker-compose up```

Access localhost:3000/ to see the main page.


## RVM Environment

Install rvm and ruby-2.4.2+

Run```bundle install``` and then ```rails s``` to run the application.



## Running test-suite

```rails test```
