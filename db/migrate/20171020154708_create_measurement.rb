class CreateMeasurement < ActiveRecord::Migration[5.1]
  def change
    create_table :measurements do |t|
      t.column :capability, :string
      t.column :value, :float
      t.column :date, :datetime
      t.column :uuid, :string
      t.column :sensor_data, :jsonb
    end
  end
end
