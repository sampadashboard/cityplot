# Cityplot

![build](https://gitlab.com/sampadashboard/cityplot/badges/master/build.svg)
![coverage](https://gitlab.com/sampadashboard/cityplot/badges/master/coverage.svg)

Cityplot project is set of tools and scripts that allows you to explore and vizualize data from InterSCity
platform with Kibana and ElasticSearch.

### What we offer?

 - A crawler that constantly fetches data from an InterSCity platform asyncronously
 - An instance of Elastic Search that keeps all fetched data
 - A instance of Kibana with features for exploration and visualization of the data

# Development

###  Dependencies

  - Docker Engine
  - Docker Compose
  - An instance of the [InterSCity Platform](https://gitlab.com/smart-city-software-platform/dev-env)

### Running Cityplot

After installing docker and docker compose and interscity platform, you can enter
in the project directory and run:

```bash
$ docker-compose up
```

Then, all the containers shall be up and running and the crawler
will start fetching measurements from your InterSCity instance to feed Elastic Search index
database. Access Kibana ``localhost:5601`` to start playing with the data
and create your visualizations.

Some important params to the execution can be changed in the ``docker-compose.yml``
file, as follows:

- **ELASTIC_HOST**: hostname or ip address of the Elastic Search machine
- **ELASTIC_PORT**: port number of the Elastic Search service
- **INDEX_NAME**: name of the index to be created on elastic search
- **PLATFORM_URL**: the URL and port where the Interscity platform is running
- **SCHDL_OFF**: a variable that turns the jobs execution on and off
- **SCHDL_TIMEOUT**: the TIME in seconds that a job has to run before being killed
- **SCHDL_TIMER**: the time in seconds to repeat the job execution

### Running Tests

To run the tests:

```bash
./scripts/tests.sh
```

To run integration set up elasticsearch before running tests
```bash
docker-compose up -d elasticsearch
```

Run the tests

```bash
 ./scripts/tests.sh --integration
```

### Demo application with Kibana

If your InterSCity application does not have any data, you can have a taste of how
things works running a demo with some fake data generated with a script.

To have a taste of the possible visualizations, get the containers up running and then
from **inside the interscity-explorer/ directory**, execute: 

```bash
sh /interscity-explorer/demo.sh
```

This will generate and post data to elastic search. Then, all you have to do is
to follow the steps illustrated in our wiki page [Kibana create visualizations](https://gitlab.com/sampadashboard/cityplot/wikis/Visualizations-with-Kibana)


# Team

Check the AUTHORS.md file to more info about our contributors.
