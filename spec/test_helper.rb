module TestHelpers

  def resource_parsed_json
    {
      :id=>4624,
      :uri=>nil,
      :created_at=>"2017-11-23T14:02:33.818Z",
      :updated_at=>"2017-11-23T14:02:33.818Z",
      :lat=>-46.7205382175293,
      :lon=>-23.5618341137784,
      :status=>"active",
      :collect_interval=>nil,
      :description=>"Testing map",
      :uuid=>"6b237ecf-c51c-43a3-a3a6-3a16abee9110",
      :city=>nil,
      :neighborhood=>nil,
      :state=>nil,
      :postal_code=>nil,
      :country=>nil
    }
  end

  def resource_raw_json
    { 
      :status => :ok,
      :resource => {
        :id=>4624,
        :uri=>nil,
        :created_at=>"2017-11-23T14:02:33.818Z",
        :updated_at=>"2017-11-23T14:02:33.818Z",
        :lat=>-46.7205382175293,
        :lon=>-23.5618341137784,
        :status=>"active",
        :collect_interval=>nil,
        :description=>"Testing map",
        :uuid=>"6b237ecf-c51c-43a3-a3a6-3a16abee9110",
        :city=>nil,
        :neighborhood=>nil,
        :state=>nil,
        :postal_code=>nil,
        :country=>nil
      }
    }
  end

  def parsed_data_json_example
    [[
      "6b237ecf-c51c-43a3-a3a6-3a16abee9110",
      { :uri=>nil,
        :created_at=>"2017-11-23T14:02:33.818Z",
        :updated_at=>"2017-11-23T14:02:33.818Z",
        :lat=>-46.7205382175293,
        :lon=>-23.5618341137784,
        :status=>"active",
        :collect_interval=>nil,
        :description=>"Testing map",
        :uuid=>"6b237ecf-c51c-43a3-a3a6-3a16abee9110",
        :city=>nil,
        :neighborhood=>nil,
        :state=>nil,
        :postal_code=>nil,
        :country=>nil}
    ]]
  end
end
