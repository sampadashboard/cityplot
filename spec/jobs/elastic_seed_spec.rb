require 'rails_helper'
require 'rails_helper'

RSpec.describe ElasticSeed, type: :job do

  describe "when elasticsearch and the platform are not available" do
    it "initialize object should not crash" do
      expect{ElasticSeed.new}.not_to raise_error
      expect(ElasticSeed.new.connection).to eq(false)
    end

    it "send_data should not crash" do
      job = ElasticSeed.new
      expect{job.send_data("any data")}.not_to raise_error
    end
  end

  #TODO integration tests
  describe "when elasticsearch and the platform are available" do
    it "initialize object should set the connection", :type => 'integration' do
      job = ElasticSeed.new
      expect(job.connection).to eq(true)
    end

    #FIXME
    it "should be able to send_data to elasticsearch", :type => 'integration' do
      job = ElasticSeed.new
      expect{job.send_data(parsed_data_json_example)}.not_to raise_error
    end
  end
end

