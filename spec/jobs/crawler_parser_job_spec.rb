require 'rails_helper'

RSpec.describe CrawlerParserJob, type: :job do

  describe "when elasticsearch and the platform are not available" do
    it "fetch_last_timestamp should return nil" do
      job = CrawlerParserJob.new
      timestamp = job.fetch_last_timestamp
      expect(timestamp).to be_nil
    end

    it "fetch_last_timestamp should not raise exception when connection failed" do
      job = CrawlerParserJob.new
      expect{job.fetch_last_timestamp}.not_to raise_error
    end

    it "fetch_sensor_data should not crash" do
      job = CrawlerParserJob.new
      sensor_data = job.fetch_sensor_data("valid-uuid")
      expect(sensor_data).to be_nil
    end

    it "fetch_sensor_data should not crash when uuid nil" do
      job = CrawlerParserJob.new
      sensor_data = job.fetch_sensor_data(nil)
      expect(sensor_data).to be_nil
    end

    it "fetch_measurements_data should not crash" do
      job = CrawlerParserJob.new
      result = job.fetch_measurements_data(nil)
      expect(result).to be_nil
    end
  end

  describe "when elasticsearch and the platform are available" do
    it "fetch_last_timestamp should return default timestamp if no entry was found" do
      job = CrawlerParserJob.new

      json_data = {"hits" => {"hits" => [{"_source" => nil }]}}
      expect(job.client).to receive(:search).and_return(json_data)

      default = 30.years.ago
      result = job.fetch_last_timestamp

      expect(result.day).to eq(default.day)
      expect(result.mon).to eq(default.mon)
      expect(result.year).to eq(default.year)
    end

    it "fetch_last_timestamp should return last timestamp from elasticsearch when an entry is found" do
      job = CrawlerParserJob.new

      one_day_ago = 1.days.ago
      json_data = {"hits" => {"hits" => [{"_source" => {"date" => one_day_ago.to_s}}]}}
      expect(job.client).to receive(:search).and_return(json_data)

      result = job.fetch_last_timestamp.to_date

      expect(result.day).to eq(one_day_ago.day)
      expect(result.mon).to eq(one_day_ago.mon)
      expect(result.year).to eq(one_day_ago.year)
    end

    it "fetch_sensor_data should parse the json when entry is found" do
      expected_json = {
          :id=>4624,
          :uri=>nil,
          :created_at=>"2017-11-23T14:02:33.818Z",
          :updated_at=>"2017-11-23T14:02:33.818Z",
          :location => {
            :lat=>-46.7205382175293,
            :lon=>-23.5618341137784
          },
          :status=>"active",
          :collect_interval=>nil,
          :description=>"Testing map",
          :uuid=>"6b237ecf-c51c-43a3-a3a6-3a16abee9110",
          :city=>nil,
          :neighborhood=>nil,
          :state=>nil,
          :postal_code=>nil,
          :country=>nil
      }

      allow(InterscitySDK::ResourceCatalogManager).to receive(:fetch).and_return(resource_raw_json)

      job = CrawlerParserJob.new
      sensor_data = job.fetch_sensor_data(expected_json[:uuid])
      expect(sensor_data).to eq(expected_json)
    end
  end
end

