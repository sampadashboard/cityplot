#!/bin/bash

## Docker install
#Below we have the steps to install docker in your computer:

## Docker
echo "-----------------"
echo "Installing docker"
echo "-----------------"
#sudo apt-get install docker

## Docker-compose
echo "--------------------------"
echo "Installing docker-compose"
echo "--------------------------"
#sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
#sudo chmod +x /usr/local/bin/docker-compose

## Testing docker
docker -v
docker-compose -v


## Ruby on Rails
echo "---------------------------------"
echo "Installing latest Ruby on Rails"
echo "---------------------------------"
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

#Install the latest stable Rails and Ruby version
curl -sSL https://get.rvm.io | bash -s stable --ruby
curl -sSL https://get.rvm.io | bash -s stable --rails


## Including path for rvm in bashrc file
echo 'export PATH="$PATH:$HOME/.rvm/bin"' >> ~/.bashrc