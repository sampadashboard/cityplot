#!/bin/bash

if test "$1" = '--integration'; then
  docker-compose run --rm -e SCHDL_OFF=OFF cityplot rspec --tag type:integration
else
  docker-compose run --rm --no-deps -e SCHDL_OFF=OFF cityplot rspec --tag ~type:integration
fi
