import sys
import random
from math import ceil, floor
from datetime import datetime, timedelta

class Capability:

	def __init__(self, name, mini, maxi, epsilon, func=None):
		self.name = name
		self.min = mini
		self.max = maxi
		self.epsilon = [epsilon, -epsilon] * 100
		if func:
			self.generate_measurement = func
		self.last = random.randint(floor(self.min), ceil(self.max))

	def get_measurement(self):
		current = max(self.min, min(self.last + random.choice(self.epsilon), self.max))
		self.last = current
		return current

random.seed()
time_now = datetime.now() + timedelta(hours=2)
one_hour = timedelta(hours=1)

try:
	n = int(sys.argv[1]) 
except:
	print("It needs one argument: the number of items per hour")
	exit()

capabilities = [Capability("temperature", 0, 36, 0.5),
				Capability("humidity", 0, 100, 1),
				Capability("air_quality", 0, 250, 5),
				Capability("luminosity", 0, 1, 0, func=lambda : random.randint(self.min, self.max)),
				Capability("noise", 0, 140, 5),
				Capability("precipitation", 0, 50, 5),
				Capability("average_speed", 0, 90, 5)]

for cap in capabilities:
	time = time_now
	with open(cap.name + ".json", "w") as file:
		for i in range(0, n):
			time_str = str(time).replace(' ', 'T')
			file.write("{\"index\": {\"_type\": \"%s\"  }}" % (cap.name))
			file.write("\n{\"uri\": \"no\", \"date\" : \"%sZ\", \"created_at\": \"%sZ\", \"updated_at\": \"%sZ\", \"location\": { \"lat\": -10, \"lon\": 20 }, \"status\": \"active\", \"collect_interval\": 2, \"description\": \"Nosso sensor\", \"uuid\": \"mdnfsdnfw334\", \"city\": \"São Paulo\", \"neighborhood\": \"Butantã\", \"state\": \"SP\", \"postal_code\": \"03032-212\", \"country\": \"Brazil\", \"capability\": \"%s\", \"%s\" : %d, \"start_range\" : \"%sZ\", \"end_range\" : \"%sZ\"}\n" % (time_str, time_str, time_str, cap.name, cap.name, cap.get_measurement(), time_str, time_str))
			time = time - one_hour
