CAPABILITIES="temperature
			  humidity
			  air_quality
			  luminosity
			  noise
			  precipitation
			  average_speed"
VISUALIZATIONS="temperature_x_time 
                line_chart_multiple_days_hourly 
                gauge-avg-temperature
                heatmap_temperature_daily
                temperature_and_humidity
                temperature_and_precipitation
                speed_x_time"
docker-compose up &
docker inspect --format 'localhost:9200' elasticsearch | xargs wget --retry-connrefused -q --wait=3 --spider
curl -X PUT 'localhost:9200/measurement' -H 'Content-Type: application/x-ndjson' --data-binary @mapping.json
python3 generate_data.py 720
for cap in $CAPABILITIES; do
  curl -H 'Content-Type: application/x-ndjson' -XPOST 'localhost:9200/measurement/_bulk?pretty' --data-binary @$cap.json
  rm $cap.json
done 
for vis in $VISUALIZATIONS; do
  curl -X PUT --data @visualizations/$vis.json "http://localhost:9200/.kibana/visualization/${vis}"
done 
